module.exports = {
  assetPrefix: process.env.NODE_ENV === 'production' ? '/blog-personal-nextjs' : '',
}
module.exports = {
    eslint: {
          dirs: ['pages', 'utils'], // Only run ESLint on the 'pages' and 'utils' directories during production builds (next build)
        },
}
